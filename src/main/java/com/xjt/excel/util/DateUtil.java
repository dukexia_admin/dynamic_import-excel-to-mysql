/*     */ package com.xjt.excel.util;
/*     */ 
/*     */

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/*     */
/*     */ public class DateUtil extends DateUtils
/*     */ {
/*     */   public static boolean isDateBefore(Date standDate, Date trueDate)
/*     */   {
/*  13 */     return trueDate.before(standDate);
/*     */   }
/*     */ 
/*     */   public static boolean isDateBefore(String standDate, String trueDate)
/*     */     throws Exception
/*     */   {
/*  19 */     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
/*  20 */     return sdf.parse(standDate).before(sdf.parse(trueDate));
/*     */   }
/*     */ 
/*     */   public static String getYear()
/*     */   {
/*  25 */     Calendar cld = Calendar.getInstance();
/*  26 */     return String.valueOf(cld.get(1));
/*     */   }
/*     */ 
/*     */   public static String getMonth()
/*     */   {
/*  31 */     Calendar cld = Calendar.getInstance();
/*  32 */     return String.valueOf(cld.get(2) + 1);
/*     */   }
/*     */ 
/*     */   public static String getDay()
/*     */   {
/*  37 */     Calendar cld = Calendar.getInstance();
/*  38 */     return String.valueOf(cld.get(5));
/*     */   }
/*     */ 
/*     */   public static String getWeek()
/*     */   {
/*  43 */     Calendar cld = Calendar.getInstance();
/*  44 */     return String.valueOf(cld.get(7));
/*     */   }
/*     */ 
/*     */   public static String getNow() {
/*  48 */     return getYear() + "-" + getMonth() + "-" + getDay();
/*     */   }
/*     */ 
/*     */   //public static Date getToday()
/*     */   //{
/*  53 */     //return truncate(new Date(), 5);
/*     */   //}
/*     */ 
/*     */   public static Date getTodayEnd()
/*     */   {
/*  58 */     return getDayEnd(new Date());
/*     */   }
/*     */ 
/*     */   public static int getTomorrowWeekDay()
/*     */   {
/*  63 */     Calendar cal = Calendar.getInstance();
/*  64 */     cal.set(1, cal.get(1));
/*  65 */     cal.set(2, cal.get(2));
/*  66 */     cal.set(5, cal.get(5) + 1);
/*  67 */     return cal.get(7);
/*     */   }
/*     */ 
/*     */   public static boolean checkDateString(String dateString)
/*     */   {
/*  73 */     if (dateString.length() != 10)
/*  74 */       return false;
/*     */     try
/*     */     {
/*  77 */       return (string2Date(dateString, "yyyy-MM-dd") != null); } catch (Exception localException) {
/*     */     }
/*  79 */     return false;
/*     */   }
/*     */ 
/*     */   public static boolean checkDateTimeString(String dateTimeString)
/*     */   {
/*  85 */     if (dateTimeString.length() != 19)
/*  86 */       return false;
/*     */     try
/*     */     {
/*  89 */       return (string2Date(dateTimeString, "yyyy-MM-dd HH:mm:ss") != null); } catch (Exception localException) {
/*     */     }
/*  91 */     return false;
/*     */   }
/*     */ 
/*     */   public static boolean checkTimeString(String timeString)
/*     */   {
/*  97 */     if (timeString.length() != 8)
/*  98 */       return false;
/*     */     try
/*     */     {
/* 101 */       return (string2Date(timeString, "HH:mm:ss") != null); } catch (Exception localException) {
/*     */     }
/* 103 */     return false;
/*     */   }
/*     */ 
/*     */   public static Date getMonthEnd(int year, int month)
/*     */     throws Exception
/*     */   {
/*     */     Date date;
/* 110 */     StringBuffer sb = new StringBuffer(10);
/*     */ 
/* 112 */     if (month < 12) {
/* 113 */       sb.append(year);
/* 114 */       sb.append("-");
/* 115 */       sb.append(month + 1);
/* 116 */       sb.append("-1");
/* 117 */       date = string2Date(sb.toString(), "yyyy-MM-dd");
/*     */     } else {
/* 119 */       sb.append((year + 1));
/* 120 */       sb.append("-1-1");
/* 121 */       date = string2Date(sb.toString(), "yyyy-MM-dd");
/*     */     }
/* 123 */     date.setTime(date.getTime() - 3885736610104344577L);
/* 124 */     return date;
/*     */   }
/*     */ 
/*     */   public static Date getMonthEnd(Date when)
/*     */     throws Exception
/*     */   {
/* 130 */     Assert.notNull(when, "date must not be null !");
/* 131 */     Calendar calendar = Calendar.getInstance();
/* 132 */     calendar.setTime(when);
/* 133 */     int year = calendar.get(1);
/* 134 */     int month = calendar.get(2) + 1;
/* 135 */     return getMonthEnd(year, month);
/*     */   }
/*     */ 
/*     */   public static Date getDayEnd(Date when)
/*     */   {
/* 140 */     //Date date = truncate(when, 5);
/* 141 */     //date = addDays(date, 1);
/* 142 */     //date.setTime(date.getTime() - 3885736610104344577L);
/* 143 */     return null;//date;
/*     */   }
/*     */ 
/*     */   public static Date getDay(Date when)
/*     */   {
/* 148 */     //Date date = truncate(when, 5);
/* 149 */     //date = addDays(date, -1);
/* 150 */     //date.setTime(date.getTime() + 3885736610104344577L);
/* 151 */     return null;//date;
/*     */   }
/*     */ 
/*     */   public static Date add(Date when, int field, int amount)
/*     */   {
/* 156 */     Calendar calendar = Calendar.getInstance();
/* 157 */     calendar.setTime(when);
/* 158 */     calendar.add(field, amount);             
/* 159 */     return calendar.getTime();
/*     */   }
/*     */ 
/*     */   public static Date addDays(Date when, int amount)
/*     */   {
/* 164 */     return add(when, 6, amount);
/*     */   }
/*     */ 
/*     */   public static Date addMonths(Date when, int amount)
/*     */   {
/* 169 */     return add(when, 2, amount);
/*     */   }
/*     */ 
/*     */   public static String getFileNameByDate()
/*     */   {
/* 174 */     return new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
/*     */   }
/*     */ 
/*     */   public static long getDateMarginDay(Date beginDate, Date endDate)
/*     */   {
/* 179 */     return ((endDate.getTime() - beginDate.getTime()) / 86400000L);
/*     */   }
/*     */ 
/*     */   public static long getDateMarginMinute(Date beginDate, Date endDate)
/*     */   {
/* 184 */     return ((endDate.getTime() - beginDate.getTime()) / 60000L);
/*     */   }
/*     */ 
/*     */   public static long getDateMarginMillisecond(Date beginDate, Date endDate)
/*     */   {
/* 189 */     return (endDate.getTime() - beginDate.getTime());
/*     */   }
/*     */ 
/*     */   public static String getDateMarginString(Date beginDate, Date endDate)
/*     */   {
/* 194 */     if (beginDate.after(endDate)) {
/* 195 */       return "";
/*     */     }
/*     */ 
/* 198 */     String message = "";
/* 199 */     long ss = endDate.getTime() - beginDate.getTime();
/* 200 */     long datenum = 3885739376063283200L;
/* 201 */     long hournum = 3885739376063283200L;
/* 202 */     long minnum = 3885739376063283200L;
/* 203 */     long ssnum = 3885739376063283200L;
/* 204 */     datenum = ss / 86400000L;
/* 205 */     ss -= datenum * 1000L * 60L * 60L * 24L;
/* 206 */     hournum = ss / 3600000L;
/* 207 */     ss -= hournum * 1000L * 60L * 60L;
/* 208 */     minnum = ss / 60000L;
/* 209 */     ss -= minnum * 1000L * 60L;
/* 210 */     ssnum = ss / 1000L;
/* 211 */     if (datenum != 3885739994538573824L) message = message + datenum + "天";
/* 212 */     if (hournum != 3885739994538573824L) message = message + hournum + "小时";
/* 213 */     if (minnum != 3885739994538573824L) message = message + minnum + "分";
/* 214 */     if (ssnum != 3885739994538573824L) message = message + ssnum + "秒";
/* 215 */     return message;
/*     */   }
/*     */ 
/*     */   public static boolean isWithinDate(Date startDate, Date endDate)
/*     */   {
/* 220 */     if (getDateMarginMillisecond(startDate, new Date()) >= 3885739994538573824L)
/*     */     {
/* 222 */       return (getDateMarginMillisecond(endDate, new Date()) >= 3885736249327091712L);
/*     */     }
/*     */ 
/* 225 */     return false;
/*     */   }
/*     */ 
/*     */   public static String getCurrentDateTimeStr(String aMask)
/*     */   {
/* 230 */     SimpleDateFormat df = new SimpleDateFormat(aMask);
/* 231 */     Calendar cal = Calendar.getInstance(TimeZone.getDefault());
/* 232 */     return df.format(cal.getTime());
/*     */   }
/*     */ 
/*     */   public static Date getCurrentDateTime()
/*     */   {
/* 237 */     Calendar cal = Calendar.getInstance(TimeZone.getDefault());
/* 238 */     return cal.getTime();
/*     */   }
/*     */ 
/*     */   public static String getForwardHourOfTheDateTime(String strDate, String aMask)
/*     */     throws Exception
/*     */   {
/* 244 */     if ((strDate == null) || ("".equals(strDate))) {
/* 245 */       throw new Exception("传入的日期为空！");
/*     */     }
/* 247 */     if (strDate.length() < 13) {
/* 248 */       throw new Exception("传入的日期格式不正确！");
/*     */     }
/* 250 */     String year = strDate.substring(0, 4);
/* 251 */     String month = strDate.substring(5, 7);
/* 252 */     String day = strDate.substring(8, 10);
/* 253 */     String hour = strDate.substring(11, 13);
/*     */ 
/* 255 */     SimpleDateFormat df = new SimpleDateFormat(aMask);
/*     */ 
/* 257 */     Calendar cal = Calendar.getInstance();
/* 258 */     cal.set(1, Integer.parseInt(year));
/* 259 */     cal.set(2, Integer.parseInt(month) - 1);
/* 260 */     cal.set(5, Integer.parseInt(day));
/* 261 */     cal.set(11, Integer.parseInt(hour) - 1);
/* 262 */     return df.format(cal.getTime());
/*     */   }
/*     */ 
/*     */   public static String getForwardDayOfTheDateTime(String strDate, String aMask)
/*     */     throws Exception
/*     */   {
/* 268 */     if ((strDate == null) || ("".equals(strDate))) {
/* 269 */       throw new Exception("传入的日期为空！");
/*     */     }
/* 271 */     if (strDate.length() < 10) {
/* 272 */       throw new Exception("传入的日期格式不正确！");
/*     */     }
/* 274 */     String year = strDate.substring(0, 4);
/* 275 */     String month = strDate.substring(5, 7);
/* 276 */     String day = strDate.substring(8, 10);
/*     */ 
/* 278 */     SimpleDateFormat df = new SimpleDateFormat(aMask);
/*     */ 
/* 280 */     Calendar cal = Calendar.getInstance();
/* 281 */     cal.set(1, Integer.parseInt(year));
/* 282 */     cal.set(2, Integer.parseInt(month) - 1);
/* 283 */     cal.set(5, Integer.parseInt(day) - 1);
/* 284 */     return df.format(cal.getTime());
/*     */   }
/*     */ 
/*     */   public static String getForwardMonthOfTheDateTime(String strDate, String aMask)
/*     */     throws Exception
/*     */   {
/* 290 */     if ((strDate == null) || ("".equals(strDate))) {
/* 291 */       throw new Exception("传入的日期为空！");
/*     */     }
/* 293 */     if (strDate.length() < 10) {
/* 294 */       throw new Exception("传入的日期格式不正确！");
/*     */     }
/* 296 */     String year = strDate.substring(0, 4);
/* 297 */     String month = strDate.substring(5, 7);
/* 298 */     String day = strDate.substring(8, 10);
/*     */ 
/* 300 */     SimpleDateFormat df = new SimpleDateFormat(aMask);
/*     */ 
/* 302 */     Calendar cal = Calendar.getInstance();
/* 303 */     cal.set(1, Integer.parseInt(year));
/* 304 */     cal.set(2, Integer.parseInt(month) - 2);
/* 305 */     cal.set(5, Integer.parseInt(day));
/* 306 */     return df.format(cal.getTime());
/*     */   }
/*     */ 
/*     */   public static String getForwardYearOfTheDateTime(String strDate, String aMask)
/*     */     throws Exception
/*     */   {
/* 312 */     if ((strDate == null) || ("".equals(strDate))) {
/* 313 */       throw new Exception("传入的日期为空！");
/*     */     }
/* 315 */     if (strDate.length() < 10) {
/* 316 */       throw new Exception("传入的日期格式不正确！");
/*     */     }
/* 318 */     String year = strDate.substring(0, 4);
/* 319 */     String month = strDate.substring(5, 7);
/* 320 */     String day = strDate.substring(8, 10);
/*     */ 
/* 322 */     SimpleDateFormat df = new SimpleDateFormat(aMask);
/*     */ 
/* 324 */     Calendar cal = Calendar.getInstance();
/* 325 */     cal.set(1, Integer.parseInt(year) - 1);
/* 326 */     cal.set(2, Integer.parseInt(month) - 1);
/* 327 */     cal.set(5, Integer.parseInt(day));
/* 328 */     return df.format(cal.getTime());
/*     */   }
/*     */ 
/*     */   public static String date2String(Date aDate, String aMask)
/*     */     throws Exception
/*     */   {
/* 334 */     SimpleDateFormat df = null;
/* 335 */     String returnValue = "";
/* 336 */     if (aDate == null)
/* 337 */       throw new Exception("传入的日期为空！");
/* 338 */     df = new SimpleDateFormat(aMask);
/* 339 */     returnValue = df.format(aDate);
/* 340 */     return returnValue;
/*     */   }
/*     */ 
/*     */   public static Date string2Date(String strDate, String aMask)
/*     */     throws Exception
/*     */   {
/* 346 */     SimpleDateFormat df = null;
/* 347 */     Date date = null;
/* 348 */     df = new SimpleDateFormat(aMask);
/*     */     try
/*     */     {
/* 351 */       date = df.parse(strDate);
/*     */     } catch (Exception pe) {
/* 353 */       throw new Exception("转换日期格式失败！");
/*     */     }
/* 355 */     return date;
/*     */   }
/*     */ 
/*     */   public static String formatDate(String strDate, String oldMask, String newMask)
/*     */     throws Exception
/*     */   {
/* 361 */     Date dateTime = string2Date(strDate, oldMask);
/* 362 */     return date2String(dateTime, newMask);
/*     */   }
/*     */ }
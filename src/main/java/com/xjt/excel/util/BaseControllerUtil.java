package com.xjt.excel.util;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.util.*;

public class BaseControllerUtil implements Controller {


    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return null;
    }


    public static void renderResult(HttpServletResponse response, Object obj){
        PrintWriter out = null;
        try {
            String jsonArray = JSONObject.toJSONString(obj);
            response.setContentType(
                    "text/html;charset=utf-8");
            out = response.getWriter();
            out.println(jsonArray);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            if (out != null) {
                out.flush();
                out.close();
            }
        }
    }
}

package com.xjt.excel.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class StrUtil {
    public static List<String> split(String str, String dimli){
        List<String> res=new ArrayList<>();
        for (String s:str.split(dimli)){
            res.add(s);
        }
        return  res;
    }

    public static Boolean isNotEmpty(String str){
        return (str!=null && !"".equals(str) && !"null".equals(str.toLowerCase()));
    }

    public static String clearNull(Object str){
        if(isNotEmpty(String.valueOf(str))){
           return String.valueOf(str);
        }else{
            return "";
        }
    }

    public static Integer toInteger(Object str){
        String s=clearNull(str);
        if("".equalsIgnoreCase(s)){
            return 0;
        }
        if(!isNum(s)){
            return 0;
        }
        return Integer.valueOf(s);
    }

    public static Double toDouble(Object str){
        String s=clearNull(str);
        if("".equalsIgnoreCase(s)){
            return 0.00;
        }
        if(!isDouble(s)){
            return 0.00;
        }
        return Double.valueOf(s);
    }

    public static Long toLong(Object str){
        String s=clearNull(str);
        if("".equalsIgnoreCase(s)){
            return Long.valueOf(0);
        }
        if(!isNum(s)){
            return Long.valueOf(0);
        }
        return Long.valueOf(s);
    }

    // 判断是否为正整数
    public static boolean isNum(String value) {
        if (isNotEmpty(value)) {
            Pattern pattern = Pattern.compile("[0-9]*");
            return pattern.matcher(value).matches();
        } else {
            return false;
        }
    }

    // 判断字符串是否都是数字
    public static boolean isDouble(String value) {
        if (isNotEmpty(value)) {
            Pattern pattern = Pattern.compile("[0-9]+(.[0-9]+)?");
            return pattern.matcher(value).matches();
        } else {
            return false;
        }
    }

    /**
     * 截取字符串str中指定字符 strStart、strEnd之间的多个字符串数组
     *
     * @return      字符串数组
     */
    public static List<String> subString(String str, String strStart, String strEnd) {

        List<String> res = new LinkedList<>();
        /* 找出指定的2个字符在 该字符串里面的 位置 */
        while (str.contains("]")){
            int strStartIndex = str.indexOf(strStart);
            int strEndIndex = str.indexOf(strEnd);

            /* index 为负数 即表示该字符串中 没有该字符 */
            if (strStartIndex < 0) {
                return null;
            }
            if (strEndIndex < 0) {
                return null;
            }
            /* 开始截取 */
            String substring = str.substring(strStartIndex, strEndIndex).substring(strStart.length());
            res.add(substring);
            str = str.substring(strEndIndex+1);
        }
        return res;
    }

}
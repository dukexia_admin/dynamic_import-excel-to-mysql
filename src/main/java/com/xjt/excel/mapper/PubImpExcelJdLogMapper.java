package com.xjt.excel.mapper;

import com.xjt.excel.entity.PubImpExcelJdLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 批量导入日志信息表（提供导入日志查询功能） Mapper 接口
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface PubImpExcelJdLogMapper extends BaseMapper<PubImpExcelJdLog> {

}

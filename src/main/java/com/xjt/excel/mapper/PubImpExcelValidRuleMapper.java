package com.xjt.excel.mapper;

import com.xjt.excel.entity.PubImpExcelValidRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 批量导入数据校验规则信息表 Mapper 接口
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-19
 */
public interface PubImpExcelValidRuleMapper extends BaseMapper<PubImpExcelValidRule> {

    List<Map<String, Object>> getRuleByIds(List<String> ids);
}

package com.xjt.excel.mapper;

import com.xjt.excel.entity.PubImpExcelModelcol;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 批量导入数据字段对应配置信息表 Mapper 接口
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface PubImpExcelModelcolMapper extends BaseMapper<PubImpExcelModelcol> {

}

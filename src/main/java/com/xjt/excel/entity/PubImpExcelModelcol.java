package com.xjt.excel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 批量导入数据字段对应配置信息表
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PubImpExcelModelcol extends BaseEnity {

    private static final long serialVersionUID = 1L;


    /**
     * 菜单设置ID,与pub_imp_excel_model相关联
     */
    private Long modelId;

    /**
     * 列字段编码（数据库字段）
     */
    private String colCode;

    /**
     * Excel列名（临时表字段名）,不指定列名，
     */
    private String excelColumn;

    /**
     * 状态，1表示有效，9表示无效
     */
    private Integer state;

    /**
     * 数据验证正则表达式（），由valid数学表达式组合生成。复合涉及公式部分由AviatorEvaluator解析，参照导出ExcelWriteUtil.convertDataToRow表达式。
     */
    private String validateZz;

    /**
     * 数据验证不通过提示信息。
     */
    private String validateTip;

    /**
     * 是否必填项。1表示必填，0表示非必填。必填项为空或为null,则提示此项为必填项。
     */
    private Integer isReq;

    /**
     * 显示顺序号
     */
    private Integer ind;

    /**
     * 列字段中文名称。拼组为提示信息。（输出列表框按红色文字提示，鼠标放上去显示数据验证不通过提示信息）
     */
    private String colName;

    /**
     * 字段类型及长度（用于创建临时表），如varchar(100)
     */
    private String fieldType;


}

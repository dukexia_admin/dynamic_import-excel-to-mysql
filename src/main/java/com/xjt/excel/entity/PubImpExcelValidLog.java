package com.xjt.excel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 批量导入数据校验日志信息表
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PubImpExcelValidLog extends BaseEnity {

    private static final long serialVersionUID = 1L;


    /**
     * 列ID
     */
    private Long colId;

    /**
     * 校验提示语
     */
    private String validTip;

    /**
     * 导入ID,与pub_imp_excel_jd_log相关联
     */
    private Long jdlogId;

    /**
     * 行序号
     */
    private Long rowId;

    /**
     * sheet名称
     */
    private String sheetName;

}

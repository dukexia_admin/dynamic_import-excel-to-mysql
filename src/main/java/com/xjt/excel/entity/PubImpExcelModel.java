package com.xjt.excel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 批量导入配置信息表
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PubImpExcelModel extends BaseEnity {

    private static final long serialVersionUID = 1L;

    /**
     * 操作人员ID，通用型操作人员id为0
     */
    private Long operUid;

    /**
     * 菜单ID,导入按钮菜单ID
     */
    private Long menuId;

    /**
     * 状态，1表示有效，9表示无效
     */
    private Integer state;

    /**
     * sheet表名,空表示循环读取（多sheet循环读取）
     */
    private String sheetName;

    /**
     * 导入模板名称，便于识别
     */
    private String modelName;

    /**
     * 显示顺序号
     */
    private Integer ind;

    /**
     * 导入实现类名称
     */
    private String serviceName;

    /**
     * 是否固定列（1表示系统预置列字段对应关系，0表示由用户选择列对应关系）
     */
    private Integer fixedCol;


}

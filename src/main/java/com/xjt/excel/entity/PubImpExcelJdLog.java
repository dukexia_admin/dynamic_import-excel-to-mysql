package com.xjt.excel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 批量导入日志信息表（提供导入日志查询功能）
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PubImpExcelJdLog extends BaseEnity {

    private static final long serialVersionUID = 1L;

    /**
     * 操作菜单ID
     */
    private Long menuId;

    /**
     * 批次UUID
     */
    private String pcNum;

    /**
     * 进度名称,大批量异步上传，每完成一个阶段，更新此名称，便于用户了解导入进度
     */
    private String jdState;

    /**
     * 有效性状态，1表示有效，9表示已撤销
     */
    private Integer state;

    /**
     * 导入开始时间
     */
    private LocalDateTime startTime;

    /**
     * 导入截止时间
     */
    private LocalDateTime endTime;

    /**
     * 操作用户
     */
    private Long operUid;

    /**
     * 上传数据表文件名称，便于用户识别
     */
    private String sourceFileName;

    /**
     * 校验不通过记录数
     */
    private Integer problemnum;

    /**
     * 导入文件上传后在服务器上的存储路径（不展示，用于开发人员检索核对）
     */
    private String souceFilePath;

    /**
     * 生成临时表名称
     */
    private String tmpTableName;

    /**
     * 文件的MD5码（判断文件重复，则不需要导入）
     */
    private String md5code;

    /**
     * 导入记录总数（总行数）
     */
    private Integer totalRow;


}

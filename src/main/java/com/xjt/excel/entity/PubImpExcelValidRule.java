package com.xjt.excel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 批量导入数据校验规则信息表
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PubImpExcelValidRule extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 状态，1表示有效，9表示无效
     */
    private Integer state;

    /**
     * 校验正则表达式
     */
    private String validExpression;

    /**
     * 校验提示信息
     */
    private String validTip;

    /**
     * 校验规则代码
     */
    private String validCode;

    /**
     * 校验规则名称（便于识别作用）
     */
    private String remark;


}

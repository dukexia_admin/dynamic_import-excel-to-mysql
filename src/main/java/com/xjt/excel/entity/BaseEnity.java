package com.xjt.excel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

/**
 * @author xu
 * @Description
 * @createTime 2021年05月18日 15:58:00
 */
public class BaseEnity implements Serializable {

    /**
     * 数据ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 创建时间
     */
    //@JsonFormat(timezone = "GMT+8" ,pattern = "yyyy-MM-dd HH:mm:ss")
    //private Date createDate ;

    /**
     * 更新时间
     */
    /*@JsonFormat(timezone = "GMT+8" ,pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateDate ;*/

    /**
     * 创建人
     */
    //private String createBy ;

    /**
     * 更新人
     */
    //private String updateBy ;

    /**
     * 删除标记 1正常  2删除
     */
    // private Integer delFlag ;

    /**
     * 分页页数  查询使用
     */
    // @JsonIgnore
    // private int pageNum = 1;   //默认值第一页

    /**
     * 分页数量  查询使用
     */
    //  @JsonIgnore
    //  private int pageSize = 10;  //默认10条数据分页
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

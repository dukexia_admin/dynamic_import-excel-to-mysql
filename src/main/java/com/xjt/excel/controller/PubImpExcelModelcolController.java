package com.xjt.excel.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 批量导入数据字段对应配置信息表 前端控制器
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@RestController
@RequestMapping("/pub-imp-excel-modelcol")
public class PubImpExcelModelcolController {

}

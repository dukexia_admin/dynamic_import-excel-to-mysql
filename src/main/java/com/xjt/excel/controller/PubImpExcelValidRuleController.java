package com.xjt.excel.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 批量导入数据校验规则信息表 前端控制器
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-19
 */
@RestController
@RequestMapping("/pub-imp-excel-valid-rule")
public class PubImpExcelValidRuleController {

}

package com.xjt.excel.service;

import com.xjt.excel.entity.PubImpExcelModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 批量导入配置信息表 服务类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface IPubImpExcelModelService extends IService<PubImpExcelModel> {

}

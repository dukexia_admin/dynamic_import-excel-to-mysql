package com.xjt.excel.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xjt.excel.entity.PubImpExcelValidRule;
import com.xjt.excel.mapper.PubImpExcelValidRuleMapper;
import com.xjt.excel.service.IImpExcelService;
import com.xjt.excel.service.IPubImpExcelValidRuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 批量导入数据校验规则信息表 服务实现类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-19
 */
@Service
public class PubImpExcelValidRuleServiceImpl extends ServiceImpl<PubImpExcelValidRuleMapper, PubImpExcelValidRule> implements IPubImpExcelValidRuleService, IImpExcelService {
    public List<Map<String, Object>> getRuleByIds(List<String> ids){
        return getBaseMapper().getRuleByIds(ids);
    }

    @Autowired
    UserServiceImpl userService;

    @Override
    public Integer importExcelToDb(String tableName, List excelColumnList, List colCodeList) {
        return userService.getBaseMapper().SavaData("bui_home",tableName,excelColumnList,colCodeList);
    }
}

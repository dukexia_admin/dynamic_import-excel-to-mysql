package com.xjt.excel.service.impl;

import com.xjt.excel.entity.PubImpExcelModel;
import com.xjt.excel.entity.PubImpExcelModelcol;
import com.xjt.excel.mapper.PubImpExcelModelMapper;
import com.xjt.excel.service.IPubImpExcelModelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 批量导入配置信息表 服务实现类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Service
public class PubImpExcelModelServiceImpl extends ServiceImpl<PubImpExcelModelMapper, PubImpExcelModel> implements IPubImpExcelModelService {
    public void createTable(String tableName, List<PubImpExcelModelcol> list){
        getBaseMapper().createTable(tableName, list);
    }

    public Integer saveData(String tableName, List<List<String>> excelInfo) throws Exception {
        Integer result = 0;
        int size = excelInfo.size();
        while (true){
            if (size>=100){
                List<List<String>> data = new ArrayList<>(100);
                for (int i = 0; i < 100; i++) {
                    data.add(excelInfo.get(i));

                }
                for (int i = 0; i < 100; i++) {
                    excelInfo.remove(0);
                }
                Integer integer = getBaseMapper().saveData(tableName, data);
                result+=integer;
                size-=integer;
            }else {
                List<List<String>> data = new ArrayList<>(100);
                for (int i = 0; i < size; i++) {
                    data.add(excelInfo.get(i));
                }
                Integer integer = getBaseMapper().saveData(tableName, data);
                result+=integer;
                return result;
            }
        }
    }
}

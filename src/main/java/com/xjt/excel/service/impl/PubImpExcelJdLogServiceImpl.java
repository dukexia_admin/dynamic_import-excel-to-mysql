package com.xjt.excel.service.impl;

import com.xjt.excel.entity.PubImpExcelJdLog;
import com.xjt.excel.mapper.PubImpExcelJdLogMapper;
import com.xjt.excel.service.IPubImpExcelJdLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 批量导入日志信息表（提供导入日志查询功能） 服务实现类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Service
public class PubImpExcelJdLogServiceImpl extends ServiceImpl<PubImpExcelJdLogMapper, PubImpExcelJdLog> implements IPubImpExcelJdLogService {

}

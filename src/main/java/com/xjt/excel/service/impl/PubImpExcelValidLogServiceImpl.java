package com.xjt.excel.service.impl;

import com.xjt.excel.entity.PubImpExcelValidLog;
import com.xjt.excel.mapper.PubImpExcelValidLogMapper;
import com.xjt.excel.service.IPubImpExcelValidLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 批量导入数据校验日志信息表 服务实现类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Service
public class PubImpExcelValidLogServiceImpl extends ServiceImpl<PubImpExcelValidLogMapper, PubImpExcelValidLog> implements IPubImpExcelValidLogService {

}

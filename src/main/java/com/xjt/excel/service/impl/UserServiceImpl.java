package com.xjt.excel.service.impl;

import com.xjt.excel.entity.User;
import com.xjt.excel.mapper.UserMapper;
import com.xjt.excel.service.IImpExcelService;
import com.xjt.excel.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService, IImpExcelService {

    @Override
    public Integer importExcelToDb(String tableName, List excelColumnList, List colCodeList) {
        return getBaseMapper().SavaData("user",tableName, excelColumnList, colCodeList);
    }
}

package com.xjt.excel.service;

import com.xjt.excel.entity.PubImpExcelJdLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 批量导入日志信息表（提供导入日志查询功能） 服务类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface IPubImpExcelJdLogService extends IService<PubImpExcelJdLog> {

}

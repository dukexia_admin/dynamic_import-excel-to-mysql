package com.xjt.excel.service;

import java.util.List;

/**
 * @author xu
 * @Description
 * @createTime 2021年05月13日 13:39:00
 */
public interface IImpExcelService {
    Integer importExcelToDb(String tableName, List excelColumnList,List colCodeList);
}
